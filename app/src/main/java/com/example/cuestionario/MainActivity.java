package com.example.cuestionario;

import android.graphics.Color;
import android.icu.text.UnicodeSetSpanner;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {
    Button btn;
    boolean comprueba= false;
    CheckBox ch1,ch2,ch3,ch4,ch5,ch6,ch8,ch9,ch10,ch11,ch12,ch13,ch14,ch15,ch16,ch17,ch18;
    RadioButton r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,r11,r12,r13,r14,r15,r16,r17,r18,r19,r20,r21,r22,r23,r24,r25,r26,r27,r28;
    TextView tx1,co,in;
    Switch s1,s2,s3,s4,s5,s6,s7;
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn=(Button) findViewById(R.id.btnEvaluar);
        ch1=(CheckBox) findViewById(R.id.Check1);
        ch2=(CheckBox)findViewById(R.id.Check2);
        ch3=(CheckBox)findViewById(R.id.Check3);
        ch4=(CheckBox)findViewById(R.id.Check4);
        ch5=(CheckBox)findViewById(R.id.Check5);
        ch6=(CheckBox)findViewById((R.id.Check6));
        ch8=(CheckBox)findViewById(R.id.Check8);
        ch9=(CheckBox)findViewById(R.id.Check9);
        ch10=(CheckBox)findViewById(R.id.Check10);
        ch11=(CheckBox)findViewById(R.id.Check11);
        ch12=(CheckBox)findViewById(R.id.Check12);
        ch13=(CheckBox)findViewById(R.id.Check13);
        ch14=(CheckBox)findViewById(R.id.Check14);
        ch15=(CheckBox)findViewById(R.id.Check15);
        ch16=(CheckBox)findViewById(R.id.Check16);
        ch17=(CheckBox)findViewById(R.id.Check17);
        ch18=(CheckBox)findViewById(R.id.Check18);
        r1=(RadioButton)findViewById(R.id.Rb1);
        r2=(RadioButton)findViewById(R.id.Rb2);
        r3=(RadioButton)findViewById(R.id.Rb3);
        r4=(RadioButton)findViewById(R.id.Rb4);
        r5=(RadioButton) findViewById(R.id.Rb5);
        r6=(RadioButton)findViewById(R.id.Rb6);
        r7=(RadioButton)findViewById(R.id.Rb7);
        r8=(RadioButton)findViewById(R.id.Rb8);
        r9=(RadioButton)findViewById(R.id.Rb9);
        r10=(RadioButton)findViewById(R.id.Rb10);
        r11=(RadioButton)findViewById(R.id.Rb11);
        r12=(RadioButton)findViewById(R.id.Rb12);
        r13=(RadioButton)findViewById(R.id.Rb13);
        r14=(RadioButton)findViewById(R.id.Rb14);
        r15=(RadioButton)findViewById(R.id.Rb15);
        r16=(RadioButton)findViewById(R.id.Rb16);
        r17=(RadioButton)findViewById(R.id.Rb17);
        r18=(RadioButton)findViewById(R.id.Rb18);
        r19=(RadioButton)findViewById(R.id.Rb19);
        r20=(RadioButton)findViewById(R.id.Rb20);
        r21=(RadioButton)findViewById(R.id.Rb21);
        r22=(RadioButton)findViewById(R.id.Rb22);
        r23=(RadioButton)findViewById(R.id.Rb23);
        r24=(RadioButton)findViewById(R.id.Rb24);
        r25=(RadioButton)findViewById(R.id.Rb25);
        r26=(RadioButton)findViewById(R.id.Rb26);
        r27=(RadioButton)findViewById(R.id.Rb27);
        r28=(RadioButton)findViewById(R.id.Rb28);
        tx1=(TextView)findViewById(R.id.textView);
        co=(TextView) findViewById(R.id.textView11);
        in=(TextView)findViewById(R.id.textView12);
        s1=(Switch)findViewById(R.id.sw1);
        s2=(Switch)findViewById(R.id.sw2);
        s3=(Switch)findViewById(R.id.sw3);
        s4=(Switch)findViewById(R.id.sw4);
        s5=(Switch)findViewById(R.id.s5);
        s6=(Switch)findViewById(R.id.s6);
        s7=(Switch)findViewById(R.id.s7);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String texto="Campos vacios en la pregunta: ";
                String text2="Solo debe elejir una opción en la pregunta #";
               if(!comprueba){
                   if(!ch1.isChecked() && !ch2.isChecked()  ){
                       Toast.makeText(getApplicationContext(),texto+"#1",Toast.LENGTH_SHORT).show();
                   }
                   else{
                       if(ch1.isChecked() && ch2.isChecked()){
                           Toast.makeText(getApplicationContext(),text2+"1",Toast.LENGTH_SHORT).show();
                       }else {
                           if(!r1.isChecked() && !r2.isChecked() && !r3.isChecked() && !r4.isChecked()){
                               Toast.makeText(getApplicationContext(),texto+"#2",Toast.LENGTH_SHORT).show();
                           }
                           else{
                               if (!ch3.isChecked() && !ch4.isChecked()){
                                   Toast.makeText(getApplicationContext(),texto+"#3",Toast.LENGTH_SHORT).show();
                               }
                               else {
                                   if(ch3.isChecked() && ch4.isChecked()){
                                       Toast.makeText(getApplicationContext(),text2+"3",Toast.LENGTH_SHORT).show();
                                   }
                                   else {
                                       if(!r5.isChecked() && !r6.isChecked()){
                                           Toast.makeText(getApplicationContext(),texto+"#4",Toast.LENGTH_SHORT).show();
                                       }
                                       else
                                       {
                                           if (!r7.isChecked() && !r8.isChecked() && !r9.isChecked()){
                                               Toast.makeText(getApplicationContext(),texto+"#5",Toast.LENGTH_SHORT).show();
                                           }
                                           else {
                                               if(! ch5.isChecked() && !ch6.isChecked() && !ch8.isChecked()){
                                                   Toast.makeText(getApplicationContext(),texto+"#6",Toast.LENGTH_SHORT).show();
                                               }
                                               else {
                                                   if(!s5.isChecked() && !s6.isChecked() && !s7.isChecked()){
                                                       Toast.makeText(getApplicationContext(),texto+"#7",Toast.LENGTH_SHORT).show();
                                                   }
                                                   else {
                                                       if((s5.isChecked() && s6.isChecked()) || (s5.isChecked() && s7.isChecked()) || (s6.isChecked() && s7.isChecked())){
                                                           Toast.makeText(getApplicationContext(),text2+"7",Toast.LENGTH_SHORT).show();
                                                           s5.setChecked(false);
                                                           s6.setChecked(false);
                                                           s7.setChecked(false);
                                                       }
                                                       else {
                                                           if(!ch9.isChecked() && ! ch10.isChecked()){
                                                               Toast.makeText(getApplicationContext(),texto+"#8",Toast.LENGTH_SHORT).show();
                                                           }
                                                           else {
                                                               if(ch9.isChecked() && ch10.isChecked()){
                                                                   Toast.makeText(getApplicationContext(),text2+"8",Toast.LENGTH_SHORT).show();
                                                               }
                                                               else {
                                                                   if (!r10.isChecked() && !r11.isChecked() && !r12.isChecked()){
                                                                       Toast.makeText(getApplicationContext(),texto+"#9",Toast.LENGTH_SHORT).show();
                                                                   }
                                                                   else {
                                                                       if (!ch11.isChecked() && !ch12.isChecked() && !ch13.isChecked()){
                                                                           Toast.makeText(getApplicationContext(),texto+"#10",Toast.LENGTH_SHORT).show();;
                                                                       }
                                                                       else
                                                                       {
                                                                           if(ch11.isChecked() && ch12.isChecked() && ch13.isChecked() || (ch11.isChecked() && ch12.isChecked()) || (ch11.isChecked() && ch13.isChecked()) || (ch13.isChecked() && ch11.isChecked()) ){
                                                                               Toast.makeText(getApplicationContext(),text2+"10",Toast.LENGTH_SHORT).show();
                                                                           }
                                                                           else{
                                                                               if(!r13.isChecked() && !r14.isChecked() && !r15.isChecked()){
                                                                                   Toast.makeText(getApplicationContext(),texto+"#11",Toast.LENGTH_SHORT).show();
                                                                               }
                                                                               else{
                                                                                   if (!s1.isChecked() && !s2.isChecked()){
                                                                                       Toast.makeText(getApplicationContext(),texto+"#12",Toast.LENGTH_SHORT).show();
                                                                                   }
                                                                                   else {
                                                                                       if (s1.isChecked() && s2.isChecked()){
                                                                                           Toast.makeText(getApplicationContext(),text2+"12",Toast.LENGTH_SHORT).show();
                                                                                       }
                                                                                       else {
                                                                                           if(!r16.isChecked() && !r17.isChecked() && !r18.isChecked()){
                                                                                               Toast.makeText(getApplicationContext(),texto+"#13",Toast.LENGTH_SHORT).show();
                                                                                           }
                                                                                           else {
                                                                                               if ((r16.isChecked() && r17.isChecked())||(r16.isChecked() && r18.isChecked())||(r17.isChecked() && r18.isChecked())){
                                                                                                   Toast.makeText(getApplicationContext(),text2+"13",Toast.LENGTH_SHORT).show();
                                                                                                   r16.setChecked(false);
                                                                                                   r17.setChecked(false);
                                                                                                   r18.setChecked(false);
                                                                                               }else {
                                                                                                   if(!r19.isChecked() && !r20.isChecked()){
                                                                                                       Toast.makeText(getApplicationContext(),texto+"#14",Toast.LENGTH_SHORT).show();
                                                                                                   }
                                                                                                   else {
                                                                                                       if(!ch14.isChecked() && !ch15.isChecked()){
                                                                                                           Toast.makeText(getApplicationContext(),texto+"#15",Toast.LENGTH_SHORT).show();
                                                                                                       }
                                                                                                       else {
                                                                                                           if(ch14.isChecked() && ch15.isChecked()){
                                                                                                               Toast.makeText(getApplicationContext(),text2+"15",Toast.LENGTH_SHORT).show();
                                                                                                           }
                                                                                                           else {
                                                                                                               if(!r21.isChecked() && !r22.isChecked() && !r23.isChecked()){
                                                                                                                   Toast.makeText(getApplicationContext(),texto+"#16",Toast.LENGTH_SHORT).show();
                                                                                                               }
                                                                                                               else {
                                                                                                                   if ((r21.isChecked() && r22.isChecked())||(r21.isChecked() && r23.isChecked())||(r22.isChecked() && r23.isChecked())){
                                                                                                                       Toast.makeText(getApplicationContext(),text2+"16",Toast.LENGTH_SHORT).show();
                                                                                                                       r21.setChecked(false);
                                                                                                                       r22.setChecked(false);
                                                                                                                       r23.setChecked(false);
                                                                                                                   }
                                                                                                                   else {
                                                                                                                       if(!ch16.isChecked() && !ch17.isChecked() && !ch18.isChecked()){
                                                                                                                           Toast.makeText(getApplicationContext(),texto+"#17",Toast.LENGTH_SHORT).show();
                                                                                                                       }
                                                                                                                       else {
                                                                                                                           if((ch16.isChecked() && ch17.isChecked())||(ch16.isChecked() && ch18.isChecked())||(ch17.isChecked() && ch18.isChecked())){
                                                                                                                               Toast.makeText(getApplicationContext(),text2+"17",Toast.LENGTH_SHORT).show();
                                                                                                                           }
                                                                                                                           else {
                                                                                                                               if(!r24.isChecked() && !r25.isChecked()){
                                                                                                                                   Toast.makeText(getApplicationContext(),texto+"#18",Toast.LENGTH_SHORT).show();
                                                                                                                               }
                                                                                                                               else {
                                                                                                                                   if (!s3.isChecked() && !s4.isChecked()){
                                                                                                                                       Toast.makeText(getApplicationContext(),texto+"#19",Toast.LENGTH_SHORT).show();
                                                                                                                                   }
                                                                                                                                   else{
                                                                                                                                       if(s3.isChecked() && s4.isChecked()){
                                                                                                                                           Toast.makeText(getApplicationContext(),text2+"19",Toast.LENGTH_SHORT).show();
                                                                                                                                       }
                                                                                                                                       else {
                                                                                                                                           if(!r26.isChecked() && !r27.isChecked() && !r28.isChecked()){
                                                                                                                                               Toast.makeText(getApplicationContext(),texto+"#20",Toast.LENGTH_SHORT).show();
                                                                                                                                           }
                                                                                                                                           else {
                                                                                                                                               verificar();
                                                                                                                                               String correc= co.getText().toString()+correctas;
                                                                                                                                               String incorrec=in.getText().toString()+incorrectas;
                                                                                                                                               co.setText(correc);
                                                                                                                                               in.setText(incorrec);
                                                                                                                                           }
                                                                                                                                       }
                                                                                                                                   }
                                                                                                                               }
                                                                                                                           }
                                                                                                                       }
                                                                                                                   }
                                                                                                               }
                                                                                                           }
                                                                                                       }
                                                                                                   }
                                                                                               }
                                                                                           }
                                                                                       }
                                                                                   }
                                                                               }
                                                                           }
                                                                       }
                                                                   }
                                                               }
                                                           }
                                                       }
                                                   }
                                               }
                                           }
                                       }
                                   }
                               }
                           }
                       }
                   }

               }
               else {
                   limpiar();
               }
            }
        });
    }
    int correctas=0,incorrectas=0;
    public void verificar(){
        if(ch1.isChecked()){
            correctas++;
        }
        else{
            incorrectas++;
        }
        if(r3.isChecked()){
            correctas++;
        }
        else{
            incorrectas++;
        }
        if (ch4.isChecked()){
            correctas++;
        }
        else {
            incorrectas++;
        }
        if(r5.isChecked()){
         correctas++;
        }
        else {
            incorrectas++;
        }
        if(r8.isChecked()){
            correctas++;
        }
        else
        {
            incorrectas++;
        }
        if(ch6.isChecked()){
            correctas++;
        }
        else {
            incorrectas++;
        }
        if(s7.isChecked()){
            correctas++;
        }
        else {
            incorrectas++;
        }
        if(ch10.isChecked()){
            correctas++;
        }
        else {
            incorrectas++;
        }
        if(r10.isChecked()){
            correctas++;
        }
        else {
            incorrectas++;
        }
        if(ch12.isChecked()){
            correctas++;
        }
        else{
            incorrectas++;
        }
        if(r15.isChecked()){
            correctas++;
        }
        else {
            incorrectas++;
        }
        if(s1.isChecked()){
            correctas++;
        }
        else {
            incorrectas++;
        }
        if(r18.isChecked() || r16.isChecked()){
            correctas++;
        }
        else{
            incorrectas++;
        }
        if(r19.isChecked()){
            correctas++;
        }
        else {
            incorrectas++;
        }
        if(ch14.isChecked()){
            correctas++;
        }
        else {
            incorrectas++;
        }
        if(r22.isChecked()){
            correctas++;
        }
        else {
            incorrectas++;
        }
        if(ch18.isChecked()){
            correctas++;
        }
        else {
            incorrectas++;
        }
        if(r24.isChecked()){
            correctas++;
        }
        else {
            incorrectas++;
        }
        if(s3.isChecked()){
            correctas++;
        }
        else {
            incorrectas++;
        }
        if (r27.isChecked()){
            correctas++;
        }
        else {
            incorrectas++;
        }
        comprueba=true;
        btn.setText("Intentar de nuevo");
    }
    public void limpiar(){
        comprueba=false;
        ch1.setChecked(false);
        ch2.setChecked(false);
        ch3.setChecked(false);
        ch4.setChecked(false);
        ch5.setChecked(false);
        ch6.setChecked(false);
        ch8.setChecked(false);
        ch9.setChecked(false);
        ch10.setChecked(false);
        ch11.setChecked(false);
        ch12.setChecked(false);
        ch13.setChecked(false);
        ch14.setChecked(false);
        ch15.setChecked(false);
        ch16.setChecked(false);
        ch17.setChecked(false);
        ch18.setChecked(false);
        co.setText("Correctas: ");
        in.setText("Incorrectas: ");
        r1.setChecked(false);
        r2.setChecked(false);
        r3.setChecked(false);
        r4.setChecked(false);
        r5.setChecked(false);
        r6.setChecked(false);
        r7.setChecked(false);
        r8.setChecked(false);
        r9.setChecked(false);
        r10.setChecked(false);
        r11.setChecked(false);
        r12.setChecked(false);
        r13.setChecked(false);
        r14.setChecked(false);
        r15.setChecked(false);
        r16.setChecked(false);
        r17.setChecked(false);
        r18.setChecked(false);
        r19.setChecked(false);
        r20.setChecked(false);
        r21.setChecked(false);
        r22.setChecked(false);
        r23.setChecked(false);
        r24.setChecked(false);
        r25.setChecked(false);
        r26.setChecked(false);
        r27.setChecked(false);
        r28.setChecked(false);
        s1.setChecked(false);
        s2.setChecked(false);
        s3.setChecked(false);
        s4.setChecked(false);
        s5.setChecked(false);
        s6.setChecked(false);
        s7.setChecked(false);

    }
}
